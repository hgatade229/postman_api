# postman_Api



# Introductions 

The existing project is all about Postman rest api testing and test cases which is already generated in the Api's which are taken from reqres.in
this will clarify that our API's are working successfully and validations provide are correct.

# Features

we can run this api's manually by using Newman direct on the terminal will able to analyse how many API's are working and
what are the flaws in which API
also we can generate html file for better understanding

# How to run 

This project contains files as a  reqres_Rest_Api.json  and Reqres_environment.json

open the terminal in location where these files exist 
* must have install newman

provide a command to run API with test cases

 $ newman run reqres_Rest_Api.json -e Reqres_environment.json
 
want to generate html file 
 
 $ neman run reqres_Rest_Api.json -e Reqres_environment.json htmlextra
